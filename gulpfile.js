const gulp = require('gulp');
const scss = require('gulp-sass');
const connect = require('gulp-connect');
const sourcemaps = require('gulp-sourcemaps');
const autoprefixer = require('gulp-autoprefixer');

gulp.task('static', function(){
  return gulp.src('src/static/**/*')
    .pipe(gulp.dest('build/static'))
    .pipe(connect.reload());
});

gulp.task('html', function(){
  return gulp.src('src/templates/index.html')
    .pipe(gulp.dest('build'))
    .pipe(connect.reload());
});

gulp.task('finish', function(){
  return gulp.src('src/templates/finish.html')
    .pipe(gulp.dest('build'))
    .pipe(connect.reload());
});

gulp.task('css', function(){
  return gulp.src('src/styles/styles.scss')
    .pipe(sourcemaps.init())
    .pipe(scss())
    .pipe(autoprefixer({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    .pipe(sourcemaps.write('./maps'))
    .pipe(gulp.dest('build'))
    .pipe(connect.reload());
});

gulp.task('watch', function () {
  gulp.watch(['src/templates/**/*.html'], ['html', 'finish']);
  gulp.watch(['src/styles/**/*.scss'], ['css']);
  gulp.watch(['src/static/**/*'], ['static']);
});

gulp.task('connect', function() {
  connect.server({
    root: 'build',
    livereload: true
  });
});

gulp.task('build', [
  'static',
  'html',
  'finish',
  'css'
])

gulp.task('default', [
  'static',
  'html',
  'finish',
  'css',
  'watch',
  'connect'
]);